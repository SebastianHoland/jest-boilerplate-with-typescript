//import function
import { strangerDanger } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test passe
//optimize

// test()  //it can replace test() 

it('should be spot, see', () => {
    //expectation!//assertions check if it is what we think
    const strArray= ['See Spot run. See Spot Jump. Spot likes jumping. See Spot fly.'];
    
    expect( strangerDanger("See Spot run. See Spot jump. Spot likes jumping. See Spot fly") )
    .toBe([["spot", "see"], []]);

})