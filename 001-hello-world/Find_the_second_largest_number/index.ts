export function secondLargestNumber(numbers) {
    if (numbers.length === 0) {
        return 0;
    }

    if (numbers.length === 2 && numbers[0] === numbers[1]) {
        return 0;
    }
        
    const sorted = numbers.sort((a, b) => b - a);
    return sorted[1];
}

