//import function
import { secondLargestNumber } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test pass
//optimize

// test()  //it can replace test() 
it('find 40', () => {
    expect([10, 40, 30, 20, 50]).toBe(40)
})

it('find 105', function () {
    expect([25, 143, 89, 13, 105]).toBe(105)
})

it('find 23', function () {
    expect([54, 23, 11, 17, 10]).toBe(23)
})

it('find 0', function () {
    expect([1, 1]).toBe(0)
})

it('find 0', function () {
    expect([1]).toBe(0)
})

it('find 0', function () {
    expect([]).toBe(0)
})



 
