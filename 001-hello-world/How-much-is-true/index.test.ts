//import function
import { countTrue } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test passe
//optimize

// test()  //it can replace test() 

it('should find 2 trues', function() {
    const bools = [ true, false, false, true, false ]
    //expectation!//assertions check if it is what we think
    expect( countTrue( bools ) ).toBe(2) 

})

it('should find 0 trues', function() {
    const bools = [false, false, false, false]
    //expectation!//assertions check if it is what we think
    expect( countTrue( bools ) ).toBe(0) 

})

it('should find 1 trues', function() {
    const bools = [1, 0, 0, true, false]
    //expectation!//assertions check if it is what we think
    expect( countTrue( bools ) ).toBe(1) 

})

it('should find 1 trues', function() {
    const bools = ["true", 1, true, 0, false]
    //expectation!//assertions check if it is what we think
    expect( countTrue( bools ) ).toBe(1) 

})

it('should find 0 trues', function() {
    const bools = []
    //expectation!//assertions check if it is what we think
    expect( countTrue( bools ) ).toBe(0) 

})


