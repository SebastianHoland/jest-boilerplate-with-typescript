//import function
import { maximumProfit } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test passe
//optimize

// test()  //it can replace test() 

it('should profit 14', () => {
    //expectation!//assertions check if it is what we think
    expect( maximumProfit([8, 5, 12, 9, 19, 1] ) ).toBe(14) 

})

it('should profit 7', () => {
    //expectation!//assertions check if it is what we think
    expect( maximumProfit([2, 4, 9, 3, 8]) ).toBe(7) 

})

it('should profit 0', () => {
    //expectation!//assertions check if it is what we think
    expect( maximumProfit([21, 12, 11, 9, 6, 3]) ).toBe(0) 

})

