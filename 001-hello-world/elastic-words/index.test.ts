//import function
import { elasticWords } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test passe
//optimize

// test()  //it can replace test() 

it('should stetch to ANNNNA', () => {
    //expectation!//assertions check if it is what we think
    expect( elasticWords("ANNA") ).toBe("ANNNNA") 

})

it('should strecth to KAAYYYAAK', () => {
    //expectation!//assertions check if it is what we think
    expect( elasticWords("KAYAK") ).toBe("KAAYYYAAK") 

})

it('should not stretch', () => {
    //expectation!//assertions check if it is what we think
    expect( elasticWords("X") ).toBe("X") 

})

it('should not stretch', () => {
    //expectation!//assertions check if it is what we think
    expect( elasticWords("XXX") ).toBe("XXX") 

})
