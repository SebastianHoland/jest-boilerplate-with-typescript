import { youShallNotPass } from "./index" //ES Module(Standard for importing)

it('stonk should be Invalid', function() {
    
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})

it('pass word should be Invalid', function() {
    
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})

it('password should be weak', function() {
   
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})

it('11081992 should be weak', function() {
    
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})

it('mySecurePass123 should be moderate', function() {
    
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})

it('!@!pass1 should be moderate', function() {
    
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})

it('@S3cur1ty should be Strong', function() {
    
    //expectation!//assertions check if it is what we think
    expect( youShallNotPass( 'stonk' ) ).toBe('Invalid') 

})



 

11081992 -> “Weak” 
mySecurePass123 -> “Moderate” 
!@!pass1 -> “Moderate” 
@S3cur1ty -> “Strong”