//import function
import { groupInOrder } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test passe
//optimize

// test()  //it can replace test() 

it('should be [1, 3], [2, 4]', function() {
    //let num = ([1, 2, 3, 4])
    //expectation!//assertions check if it is what we think
    expect( groupInOrder( [1, 2, 3, 4], 2 ) ).toBe([[1, 3], [2, 4]]) 

})

//groupInOrder([1, 2, 3, 4], 2) ➞ [[1, 3], [2, 4]] 
 
//groupInOrder([1, 2, 3, 4, 5, 6, 7], 4) ➞ [[1, 3, 5, 7], [2, 4, 6]] 
 
//groupInOrder([1, 2, 3, 4, 5], 1) ➞ [[1], [2], [3], [4], [5]] 
 
//groupInOrder([1, 2, 3, 4, 5, 6], 4) ➞ [[1, 3, 5], [2, 4, 6]]