//

export function rearrange(sentence) {
    //Splits the sentence into an array of substrings
	return sentence.split(" ")  
        //Sort the elements in the array
		.sort((a, b) => a.replace(/\D/g, "") - b.replace(/\D/g, ""))
		.filter(Boolean)
		.map(w => w.replace(/\d/, ""))
        //return array of the string
		.join(" ");
}