import { rearrange } from "./index" //ES Module(Standard for importing)

it('Rearrange "is2 Thi1s T4est 3a" to "This is a Test', function(){
    const sentence = "is2 Thi1s T4est 3a";
    //expectation!//assertions check if it is what we think
    expect( rearrange( sentence ) ).toBe('This is a Test') 

})

it('Rearrange "4of Fo1r pe6ople g3ood th5e the2" to "For the good of the people', function(){
    const sentence = "4of Fo1r pe6ople g3ood th5e the2";
    //expectation!//assertions check if it is what we think
    expect( rearrange( sentence ) ).toBe('For the good of the people') 

})

it('Rearrange "5weird i2s JavaScri1pt dam4n so3" to "JavaScript is so damn weird', function(){
    const sentence = "5weird i2s JavaScri1pt dam4n so3";
    //expectation!//assertions check if it is what we think
    expect( rearrange( sentence ) ).toBe('JavaScript is so damn weird') 

})

it('Rearrange "" to "', function(){
    const sentence = "";
    //expectation!//assertions check if it is what we think
    expect( rearrange( sentence ) ).toBe('') 

})

it('Rearrange "studying3 I1 a2m" to "I am studying', function(){
    const sentence = "studying3 I1 a2m";
    //expectation!//assertions check if it is what we think
    expect( rearrange( sentence ) ).toBe('I am studying') 


})
 
 
 
 
 
//rearrange(" ") ➞ ""