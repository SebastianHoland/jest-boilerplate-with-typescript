//import function
import { points } from "./index" //ES Module(Standard for importing)

// test code to test function 3 phases
//test that fails
//make test passe
//optimize

// test()  //it can replace test() 
it('find 5', function () {
    const num1 = 1
    const num2 = 1
    expect( points(num1, num2) ).toBe(5)
})

it('find 29', function () {
    const num1 = 7
    const num2 = 5
    expect( points(num1, num2) ).toBe(29)
})

it('find 100', function () {
    const num1 = 38
    const num2 = 8
    expect( points(num1, num2) ).toBe(100)
})

it('find 3', function () {
    const num1 = 0
    const num2 = 1
    expect( points(num1, num2) ).toBe(3)
})

it('find 0', function () {
    const num1 = 0
    const num2 = 0
    expect( points(num1, num2) ).toBe(0)
})
